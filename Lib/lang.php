<?php
/**
 * COmanage Registry KDC Provisioner Plugin Language File
 *
 */
  
global $cm_lang, $cm_texts;

// When localizing, the number in format specifications (eg: %1$s) indicates the argument
// position as passed to _txt.  This can be used to process the arguments in
// a different order than they were passed.

$cm_kdc_provisioner_texts['en_US'] = array(
  // Titles, per-controller
  'ct.co_kdc_provisioner_targets.1'  => 'Kdc Provisioner Target',
  'ct.co_kdc_provisioner_targets.pl' => 'Kdc Provisioner Targets',
  
  // Error messages
  'er.kdcprovisioner.servername'        => 'Server error',
 // 'er.changelogprovisioner.logfile.lock'   => 'Failed to obtain lock on logfile "%1$s"',
  
  // Plugin texts
  'pl.kdcprovisioner.info'         => ' Kerberos server details configuration.',
  'pl.kdcprovisioner.servername'      => 'Kerberos admin server name',
  'pl.kdcprovisioner.servername.desc' => 'connect to kerberos server',
  'pl.kdcprovisioner.realm'       => 'Realm',
  'pl.kdcprovisioner.realm.desc'  => 'kerberos server Realm name',
  'pl.kdcprovisioner.password'       => 'Password',
  'pl.kdcprovisioner.password.desc'  => 'Password to use for authentication'

);
