<?php
/**
 * COmanage Registry Kdc Provisioner Model
 */
class KdcProvisioner extends AppModel {
  // Required by COmanage Plugins
  public $cmPluginType = "provisioner";

  // Document foreign keys
  public $cmPluginHasMany = array();
  
  public function cmPluginMenus() {
  	return array();
  }
}

