<?php
/**
 * COmanage Registry CO Changelog Provisioner Target Model
 *
 */
App::uses("CoProvisionerPluginTarget", "Model");

class CoKdcProvisionerTarget extends CoProvisionerPluginTarget {
  // Define class name for cake
  public $name = "CoKdcProvisionerTarget";
  
  // Add behaviors
  public $actsAs = array('Containable');
  
  // Association rules from this model to other models
  public $belongsTo = array("CoProvisioningTarget");
  
  // Default display field for cake generated views
  #public $displayField = "logfile";
  Public $displayField = "servername";
  
  // Validation rules for table elements
  public $validate = array(
    'co_provisioning_target_id' => array(
      'rule' => 'numeric',
      'required' => true,
      'message' => 'A CO Provisioning Target ID must be provided'
    ),
    'servername' => array(
      'rule' => 'notBlank',
      'required' => true,
      'allowEmpty' => false,
      'message' => 'Please enter a kerberos admin server name'
    ),
    'realm' => array(
      'rule' => 'notBlank',
      'required' => true,
      'allowEmpty' => false
    ),
    'password' => array(
      'rule' => 'notBlank',
      'required' => true,
      'allowEmpty' => false
    )
    );
    
  /**
   * Provision for the specified CO Person.
   *
   * @param  Array CO Provisioning Target data
   * @param  ProvisioningActionEnum Registry transaction type triggering provisioning
   * @param  Array Provisioning data, populated with ['CoPerson'] or ['CoGroup']
   * @return Boolean True on success
   * @throws RuntimeException
   */
  
  public function provision($coProvisioningTargetData, $op, $provisioningData) {
		  $servername = $coProvisioningTargetData['CoKdcProvisionerTarget']['servername'];
	  $realm = $coProvisioningTargetData['CoKdcProvisionerTarget']['realm'];
	  $password = $coProvisioningTargetData['CoKdcProvisionerTarget']['password'];
	  $this->log("servername is======" . print_r($servername, true));
	  $this->log("realm is======" . print_r($realm, true));
	 # $this->log("admin password is======" . print_r($password, true));

#	  $this->log("HarshadData is****************** " . print_r($provisioningData, true));
	  $this->log("identifier is======" . print_r($provisioningData['Identifier'],true));
	  $KerberosIdentifier=null;
	  foreach($provisioningData['Identifier'] as $identifier) {

      if($identifier['type'] == 'krbPrincipalName') {
        $KerberosIdentifier = $identifier['identifier'];
        break;
      }
	  }
	  $this->log("Kerberos principal=========" . print_r($KerberosIdentifier, true));
	 # $KerberosIdentifier =  $provisioningData['Identifier'][1]['identifier'];
	 # $this->log("Kerberos principal=========" . print_r($KerberosIdentifier, true));
	 # $this->log("Kdc Authenticator password is======" . print_r($provisioningData['Kdc'][0]['password'],true));
	  $KerberosPassword = null;
	  $KerberosPassword = $provisioningData['Kdc'][0]['password'];
	  
           $config = array(
	'realm' => $realm,
	'admin_server' => $servername
);
	 # $KerberosIdentifier=$KerberosIdentifier."@IUCAA.IN";
	  $conn = new KADM5($realm, $password, false ,$config);
	  $PrincExist=false;

	  try {
	    $conn->getPrincipal($KerberosIdentifier);	
		$PrincExist=true;
	  }
	  catch(Exception $e)
	  {

	  }	  
	  if($PrincExist==false)
	  {
		$princ = new KADM5Principal($KerberosIdentifier);
	$conn->createPrincipal($princ, $KerberosPassword);
	$princ->save();
	  }
	  else
	  {
	$princ2 = $conn->getPrincipal($KerberosIdentifier);
	$princ2->changePassword($KerberosPassword);
#	$princ2->save();
	  }

	  return true;
  }
}
