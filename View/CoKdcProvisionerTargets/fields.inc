<!--
/**
 * COmanage Registry CO Kdc Provisioning Target Fields
 */
-->
<?php
  // Determine if fields are editable
  $e = false;
  
  if(($this->action == "add" && $permissions['add']) || ($this->action == "edit" && $permissions['edit']))
    $e = true;
    
  // We shouldn't get here if we don't have at least read permission, but check just in case
  
  if(!$e && !$permissions['view'])
    return false;
  
  print $this->Form->hidden('co_id', array('default' => $cur_co['Co']['id'])) . "\n";
  print $this->Form->hidden('co_provisioning_target_id', array('default' => $vv_ptid)) . "\n";
?>
<div class="co-info-topbox">
  <i class="material-icons">info</i>
  <?php print _txt('pl.kdcprovisioner.info'); ?>
</div>
<ul id="<?php print $this->action; ?>_co_kdc_provisioner_target" class="fields form-list form-list-admin">
  <li>
    <div class="field-name">
      <div class="field-title">
        <?php print _txt('pl.kdcprovisioner.servername'); ?>
        <span class="required">*</span>
      </div>
      <div class="field-desc"><?php print _txt('pl.kdcprovisioner.servername.desc'); ?></div>
    </div>
    <div class="field-info">
      <?php print ($e ? $this->Form->input('servername', array('size' => 50)) : filter_var($co_kdc_provisioner_targets[0]['CoKdcProvisionerTarget']['servername'],FILTER_SANITIZE_SPECIAL_CHARS)); ?>
    </div>
  </li>
  <li>
    <div class="field-name">
      <div class="field-title"><?php print _txt('pl.kdcprovisioner.realm'); ?> <span class="required">*</span></div>
      <div class="field-desc"><?php print _txt('pl.kdcprovisioner.realm.desc'); ?></div>
    </div>
    <div class="field-info">
      <?php print ($e ? $this->Form->input('realm', array('size' => 50)) : filter_var($co_kdc_provisioner_targets[0]['CoKdcProvisionerTarget']['realm'],FILTER_SANITIZE_SPECIAL_CHARS)); ?>
    </div>
  </li>
  <li>
    <div class="field-name">
      <div class="field-title"><?php print _txt('pl.kdcprovisioner.password'); ?> <span class="required">*</span></div>
      <div class="field-desc"><?php print _txt('pl.kdcprovisioner.password.desc'); ?></div>
    </div>
    <div class="field-info">
      <?php print ($e ? $this->Form->input('password') : filter_var($co_kdc_provisioner_targets[0]['CoKdcProvisionerTarget']['password'],FILTER_SANITIZE_SPECIAL_CHARS)); ?>
    </div>
  </li>

  <?php if($e): ?>
    <li class="fields-submit">
      <div class="field-name">
        <span class="required"><?php print _txt('fd.req'); ?></span>
      </div>
      <div class="field-info">
        <?php print $this->Form->submit($submit_label); ?>
      </div>
    </li>
  <?php endif; ?>
</ul>
